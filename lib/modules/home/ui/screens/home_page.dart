import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:project_x/const/app_colors.dart';
import 'package:project_x/const/app_images.dart';
import 'package:project_x/const/app_style.dart';
import 'package:project_x/routess/routess.dart';

class HomePage extends StatelessWidget {
  HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return AutoTabsScaffold(
      backgroundColor: AppColors.bg,
      routes: [
        EmployeesPageRoute(),
        EmployeesPageRoute(),
        EmployeesPageRoute(),
        EmployeesPageRoute(),
      ],
      bottomNavigationBuilder: (_, tabsRouter) {
        return BottomNavigationBar(
          currentIndex: tabsRouter.activeIndex,
          onTap: tabsRouter.setActiveIndex,
          type: BottomNavigationBarType.fixed,
          backgroundColor: AppColors.black,
          selectedItemColor: AppColors.purple,
          unselectedItemColor: AppColors.greyText,
          unselectedLabelStyle: AppFonts.w700s12,
          items: const [
            BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage(AppImages.people),
                size: 24,
              ),
              label: 'Сотрудники',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage(AppImages.element),
                size: 24,
              ),
              label: 'Сервисы',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage(AppImages.book),
                size: 24,
              ),
              label: 'Кандидаты',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage(AppImages.profile),
                size: 24,
              ),
              label: 'Профиль',
            ),
          ],
        );
      },
    );
  }
}
