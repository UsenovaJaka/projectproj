// import 'package:json_annotation/json_annotation.dart';

// part 'auth_model.g.dart';

// @JsonSerializable(explicitToJson: true)
// class AuthModel {
//   final String? accessToken;
//   final String? refreshToken;
//   final String? username;
//   final String? tokenType;

//   AuthModel({
//     this.accessToken,
//     this.refreshToken,
//     this.username,
//     this.tokenType,
//   });

//   factory AuthModel.fromJson(Map<String, dynamic> json) =>
//       _$AuthModelFromJson(json);

//   Map<String, dynamic> toJson() => _$AuthModelToJson(this);
// }

import 'package:json_annotation/json_annotation.dart';
part 'auth_model.g.dart';

@JsonSerializable()
class AuthModel {
  String? status;
  Data? data;
  String? message;

  AuthModel({this.status, this.data, this.message});

  factory AuthModel.fromJson(Map<String, dynamic> json) =>
      _$AuthModelFromJson(json);
}

@JsonSerializable()
class Data {
  String? accessToken;
  String? refreshToken;
  String? username;
  String? tokenType;

  Data({this.accessToken, this.refreshToken, this.username, this.tokenType});

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);
}
