import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/widgets.dart';
import 'package:project_x/const/app_colors.dart';
import 'package:project_x/const/app_style.dart';

class TopShowBar {
  static connectionErrorShowBar(BuildContext context,
      {required String title, required String messageText}) {
    return Flushbar(
      titleText: Text(
        title,
        style: AppFonts.w700s16.copyWith(color: AppColors.red),
      ),
      messageText: Text(
        messageText,
        style: AppFonts.w600s14.copyWith(color: AppColors.white),
      ),
      backgroundColor: AppColors.bg2,
      borderColor: AppColors.showBarBorder,
      flushbarPosition: FlushbarPosition.TOP,
      margin: EdgeInsets.all(20),
      borderRadius: BorderRadius.circular(12),
      duration: Duration(milliseconds: 3000),
    ).show(context);
  }
}
