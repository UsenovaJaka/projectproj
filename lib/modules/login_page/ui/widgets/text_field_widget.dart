import 'package:flutter/material.dart';
import 'package:project_x/const/app_colors.dart';
import 'package:project_x/const/app_images.dart';
import 'package:project_x/const/app_style.dart';

class CustomTextField extends StatefulWidget {
  final String hintText;
  final String? errorText;
  final bool obscureText;
  final FocusNode? focusNode;
  final TextEditingController controller;
  final FormFieldValidator<String> validator;
  final Function(String)? onFieldSubmitted;

  const CustomTextField({
    Key? key,
    required this.hintText,
    this.obscureText = false,
    required this.controller,
    required this.validator,
    this.focusNode,
    this.onFieldSubmitted,
    this.errorText,
  }) : super(key: key);

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  bool _showClearIcon = false;
  bool _obscureText = true;
  String? _errorText;
  bool isFocused = false;

  @override
  void initState() {
    super.initState();

    widget.controller.addListener(() {
      setState(() {
        _showClearIcon = widget.controller.text.isNotEmpty;
      });
    });
  }

  // @override
  // void dispose() {
  //   widget.controller.dispose();
  //   super.dispose();
  // }

  void _clearText() {
    setState(() {
      widget.controller.clear();
      _showClearIcon = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onTap: () => setState(() {
        isFocused = true;
      }),
      onTapOutside: (value) => setState(() {
        isFocused = false;
      }),
      focusNode: widget.focusNode,
      onFieldSubmitted: widget.onFieldSubmitted,
      textInputAction: TextInputAction.next,
      style: AppFonts.w600s16.copyWith(color: AppColors.white),
      controller: widget.controller,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.all(16),
        filled: true,
        fillColor: AppColors.bg2,
        hintText: widget.hintText,
        hintStyle: AppFonts.w600s16.copyWith(color: AppColors.greyText),
        suffixIcon: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            _showClearIcon
                ? isFocused
                    ? Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: InkWell(
                          child: ImageIcon(
                            AssetImage(AppImages.close),
                            color: AppColors.greyText,
                          ),
                          onTap: _clearText,
                        ),
                      )
                    : SizedBox()
                : SizedBox(),
            widget.obscureText
                ? IconButton(
                    splashRadius: 1,
                    icon: ImageIcon(
                      _obscureText
                          ? AssetImage(AppImages.eye)
                          : AssetImage(AppImages.eye_slash),
                      color: AppColors.greyText,
                    ),
                    onPressed: () {
                      setState(() {
                        _obscureText = !_obscureText;
                      });
                    },
                  )
                : SizedBox(),
          ],
        ),
        errorText: _errorText,
        errorStyle: AppFonts.w600s14.copyWith(color: Colors.red),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12),
          borderSide: BorderSide(color: Colors.red),
        ),
        focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: BorderSide(color: AppColors.purple, width: 1)),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: BorderSide(color: AppColors.purple)),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(color: AppColors.bg2)),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(color: AppColors.bg2)),
      ),
      obscureText: widget.obscureText && _obscureText,
      validator: widget.validator,
    );
  }
}
