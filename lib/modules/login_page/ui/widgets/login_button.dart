import 'package:flutter/material.dart';
import 'package:project_x/const/app_colors.dart';
import 'package:project_x/const/app_style.dart';

class AppElevatedButton extends StatelessWidget {
  AppElevatedButton({
    Key? key,
    required this.title,
    required this.onPressed,
    this.focusNode,
  }) : super(key: key);
  final String title;
  final Function() onPressed;
  final FocusNode? focusNode;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      focusNode: focusNode,
      style: ElevatedButton.styleFrom(
        padding: EdgeInsets.symmetric(vertical: 16),
        foregroundColor: AppColors.bg2,
        backgroundColor: AppColors.purple,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(50),
        ),
      ),
      onPressed: onPressed,
      child: Text(
        title,
        style: AppFonts.w700s16.copyWith(color: AppColors.white),
      ),
    );
  }
}
