import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project_x/const/app_colors.dart';
import 'package:project_x/const/app_images.dart';
import 'package:project_x/const/app_style.dart';
import 'package:project_x/injectable/injectable.dart';
import 'package:project_x/modules/login_page/core/bloc/auth_bloc.dart';

import 'package:project_x/modules/login_page/ui/widgets/flushbar_top.dart';
import 'package:project_x/modules/login_page/ui/widgets/login_button.dart';
import 'package:project_x/modules/login_page/ui/widgets/text_field_widget.dart';
import 'package:project_x/routess/routess.gr.dart';

class LoginPage extends StatefulWidget {
  LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController controllerEmail = TextEditingController();

  final TextEditingController controllerPassword = TextEditingController();

  final formKey = GlobalKey<FormState>();

  FocusNode _textField1FocusNode = FocusNode();

  FocusNode _textField2FocusNode = FocusNode();

  FocusNode _buttonFocusNode = FocusNode();

  @override
  void dispose() {
    controllerEmail.dispose();
    controllerPassword.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => sl<AuthBloc>(),
      child: Scaffold(
        backgroundColor: AppColors.bg,
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Form(
            key: formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image.asset(AppImages.wedevs),
                SizedBox(height: 50),
                CustomTextField(
                  focusNode: _textField1FocusNode,
                  onFieldSubmitted: (value) {
                    FocusScope.of(context).requestFocus(_textField2FocusNode);
                  },
                  hintText: 'Логин',
                  controller: controllerEmail,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Введите логин';
                    } else if (value.contains(' ')) {}
                    return null;
                  },
                ),
                SizedBox(height: 24),
                CustomTextField(
                  focusNode: _textField2FocusNode,
                  onFieldSubmitted: (value) {
                    FocusScope.of(context).requestFocus(_buttonFocusNode);
                  },
                  hintText: 'Пароль',
                  controller: controllerPassword,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Введите пароль';
                    } else if (value.contains(' ')) {}
                    return null;
                  },
                  obscureText: true,
                ),
                SizedBox(height: 16),
                Row(
                  children: [
                    TextButton(
                      onPressed: () {},
                      child: Text(
                        'Забыли пароль?',
                        style: AppFonts.w700s14.copyWith(
                          color: AppColors.purple,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 24),
                BlocConsumer<AuthBloc, AuthState>(
                  listener: (context, state) {
                    if (state is AuthError) {
                      TopShowBar.connectionErrorShowBar(context,
                          title: 'Ошибка', messageText: 'Аккаунт не найден');
                    }
                    ;
                    if (state is AuthSuccess) {
                      context.router.pushAndPopUntil(HomePageRoute(),
                          predicate: (_) => false);
                    }
                  },
                  builder: (context, state) {
                    return SizedBox(
                        width: double.infinity,
                        child: AppElevatedButton(
                          focusNode: _buttonFocusNode,
                          title: "Войти",
                          onPressed: () {
                            if (formKey.currentState?.validate() == true) {
                              final username = controllerEmail.text;
                              final password = controllerPassword.text;
                              BlocProvider.of<AuthBloc>(context)
                                  .add(EnterLoginEvent(username, password));
                            }
                          },
                        ));
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}









// showDialog(
            //   context: context,
            //   builder: (context) => AlertDialog(
            //     title: Text(''),
            //     content: Text(state.errorMessage),
            //     actions: [
            //       TextButton(
            //         onPressed: () => Navigator.pop(context),
            //         child: Text('OK'),
            //       ),
            //     ],
            //   ),
            // );