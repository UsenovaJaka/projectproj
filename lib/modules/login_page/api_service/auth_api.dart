import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:project_x/modules/login_page/models/auth_model.dart';
import 'package:retrofit/retrofit.dart';

part 'auth_api.g.dart';

@lazySingleton
@RestApi()
abstract class ApiClient {
  @factoryMethod
  factory ApiClient(Dio dio) = _ApiClient;

  @POST('/login')
  Future<AuthModel> login(
    @Field() String username,
    @Field() String password,
  );
}
