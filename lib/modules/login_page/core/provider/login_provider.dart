// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:injectable/injectable.dart';

import 'package:project_x/modules/login_page/api_service/auth_api.dart';
import 'package:project_x/modules/login_page/core/encrypt/encryption.dart';
import 'package:project_x/modules/login_page/models/auth_model.dart';

@lazySingleton
class LoginProvider {
  final ApiClient _client;
  final FlutterSecureStorage _secureStorage;

  LoginProvider(
    this._client,
    this._secureStorage,
  );

  Future<AuthModel> login(String username, String password) async {
    try {
      final encryptLogin = LoginEncrypt();
      final response = await _client.login(
        username,
        encryptLogin.cipher(password),
      );

      await _secureStorage.write(
        key: 'accessToken',
        value: response.data?.accessToken,
      );
      await _secureStorage.write(
        key: 'refreshToken',
        value: response.data?.refreshToken,
      );

      return response;
    } catch (error) {
      throw Exception('Failed to logijjjjjn: $error');
    }
  }
}
