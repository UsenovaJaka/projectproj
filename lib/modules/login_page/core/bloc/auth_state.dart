part of 'auth_bloc.dart';

@immutable
abstract class AuthState {}

class AuthInitial extends AuthState {}

class AuthLoading extends AuthState {}

class AuthSuccess extends AuthState {
  final AuthModel response;

  AuthSuccess(this.response);
}

class AuthError extends AuthState {
  final String errorText;

  AuthError(this.errorText);

  String get errorMessage => 'акк не найден';
}
