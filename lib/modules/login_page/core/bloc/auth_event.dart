part of 'auth_bloc.dart';

@immutable
abstract class AuthEvent {}

class EnterLoginEvent extends AuthEvent {
  final String username;
  final String password;
  EnterLoginEvent(this.username, this.password);
}
