import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:project_x/modules/login_page/core/repository/auth_repo.dart';
import 'package:project_x/modules/login_page/models/auth_model.dart';

part 'auth_event.dart';
part 'auth_state.dart';

@injectable
class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthRepository _authRepository;
  AuthBloc(this._authRepository) : super(AuthInitial()) {
    on<EnterLoginEvent>((event, emit) async {
      try {
        final authResponse =
            await _authRepository.login(event.username, event.password);

        emit(AuthSuccess(authResponse));
      } catch (error) {
        emit(
          AuthError(
            error.toString(),
          ),
        );
      }
    });
  }
}
