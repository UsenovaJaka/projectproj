import 'dart:convert';

import 'package:encrypt/encrypt.dart' as encrypt;

class LoginEncrypt {
  final keyUtf8 = '5v8y/B?E(H+KbPeS';
  final ivUtf8 = 'RfUjXn2r4u7x!A%D';
  String cipher(String password) {
    final key = encrypt.Key.fromUtf8(keyUtf8);
    final iv = encrypt.IV.fromUtf8(ivUtf8);
    final encrypter =
        encrypt.Encrypter(encrypt.AES(key, mode: encrypt.AESMode.cbc));
    final encrypted = encrypter.encrypt(password, iv: iv);
    return encrypted.base64;
  }

  String decrypter(String base64String) {
    final key = encrypt.Key.fromUtf8(keyUtf8);
    final iv = encrypt.IV.fromUtf8(ivUtf8);
    final encrypter =
        encrypt.Encrypter(encrypt.AES(key, mode: encrypt.AESMode.cbc));
    final base64Decoder = base64.decoder;

    final decrypter = encrypter.decrypt(
        encrypt.Encrypted(base64Decoder.convert(base64String)),
        iv: iv);
    return decrypter;
  }
}
