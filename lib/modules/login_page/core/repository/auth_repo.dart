import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'package:injectable/injectable.dart';
import 'package:project_x/modules/login_page/core/provider/login_provider.dart';
import 'package:project_x/modules/login_page/models/auth_model.dart';

@lazySingleton
class AuthRepository {
  final LoginProvider _provider;

  AuthRepository(
    this._provider,
  );

  Future<AuthModel> login(String username, String password) async {
    try {
      return await _provider.login(username, password);
    } catch (e) {
      rethrow;
    }
  }
}
