import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:project_x/const/app_colors.dart';
import 'package:project_x/const/app_images.dart';
import 'package:project_x/const/app_style.dart';
import 'package:project_x/routess/routess.dart';

class ProfileCard extends StatelessWidget {
  const ProfileCard({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 16, bottom: 10),
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: AppColors.bg2,
      ),
      child: Column(
        children: [
          Row(
            children: [
              const CircleAvatar(
                backgroundImage: AssetImage(AppImages.avatar),
                radius: 35,
              ),
              const SizedBox(
                width: 16,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Нурдин кызы Бегайым',
                    style: AppFonts.w700s18.copyWith(color: AppColors.white),
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Row(
                    children: [
                      Text(
                        "UX/UI дизайнер",
                        style: AppFonts.w400s14
                            .copyWith(color: AppColors.cardText),
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Text(
                        '·',
                        style:
                            AppFonts.w600s14.copyWith(color: AppColors.white),
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Text(
                        "Intern",
                        style: AppFonts.w400s14
                            .copyWith(color: AppColors.cardText),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          InkWell(
              child: Row(
                children: [
                  const ImageIcon(
                    AssetImage(AppImages.more),
                    color: AppColors.purple,
                    size: 23,
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Text(
                    'Подробнее',
                    style: AppFonts.w700s14.copyWith(color: AppColors.purple),
                  ),
                ],
              ),
              onTap: () => context.router.push(ProfileDescriptionRoute())),
        ],
      ),
    );
  }
}
