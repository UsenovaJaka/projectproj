import 'package:flutter/material.dart';
import 'package:project_x/const/app_colors.dart';
import 'package:project_x/const/app_images.dart';
import 'package:project_x/modules/profile_page/ui/widgets/contact_info.dart';

class TabBarSecond extends StatelessWidget {
  const TabBarSecond({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Container(
          width: double.infinity,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: AppColors.bg2,
          ),
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: const [
                Contact_info(
                  image: AppImages.call,
                  info: '+996 709 815 969',
                ),
                SizedBox(
                  height: 22,
                ),
                Contact_info(
                  image: AppImages.whatsapp,
                  info: '+996 709 815 969',
                ),
                SizedBox(
                  height: 22,
                ),
                Contact_info(
                  image: AppImages.telegram,
                  info: '@begaiym',
                ),
                SizedBox(
                  height: 22,
                ),
                Contact_info(
                  image: AppImages.mail,
                  info: 'example@gmail.com',
                ),
                SizedBox(
                  height: 22,
                ),
                Contact_info(
                  image: AppImages.linkedin,
                  info: 'Begaiym Nurdin kyzy',
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
