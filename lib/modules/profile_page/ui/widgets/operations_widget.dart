import 'package:flutter/material.dart';
import 'package:project_x/const/app_colors.dart';
import 'package:project_x/const/app_images.dart';
import 'package:project_x/const/app_style.dart';

class OperationsCardWidget extends StatelessWidget {
  const OperationsCardWidget({
    super.key,
    required this.date,
    required this.sum,
    required this.card,
    required this.title,
  });

  final String date;
  final String sum;
  final String card;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      width: double.infinity,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12), color: AppColors.bg2),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                date,
                style: AppFonts.w400s14.copyWith(color: AppColors.white),
              ),
              const Spacer(),
              Image.asset(AppImages.logo),
              const SizedBox(
                width: 5,
              ),
              Text(
                card,
                style: AppFonts.w400s14.copyWith(color: AppColors.white),
              )
            ],
          ),
          const SizedBox(
            height: 8,
          ),
          Text('$sum KGS',
              style: AppFonts.w700s16.copyWith(color: AppColors.white)),
          const SizedBox(
            height: 8,
          ),
          Text(title,
              style: AppFonts.w400s14.copyWith(color: AppColors.greyText))
        ],
      ),
    );
  }
}
