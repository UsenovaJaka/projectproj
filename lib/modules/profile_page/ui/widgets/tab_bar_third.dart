import 'package:flutter/material.dart';
import 'package:project_x/const/app_colors.dart';
import 'package:project_x/const/app_images.dart';

import 'package:project_x/const/app_style.dart';

class TabBarThird extends StatelessWidget {
  const TabBarThird({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 12).copyWith(top: 12),
          width: double.infinity,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: AppColors.bg2,
          ),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Статус",
                  style: AppFonts.w600s12.copyWith(color: AppColors.greyText),
                ),
                const SizedBox(
                  height: 4,
                ),
                Text(
                  "На работе",
                  style: AppFonts.w600s16.copyWith(color: AppColors.white),
                ),
                const SizedBox(
                  height: 12,
                ),
                Text(
                  "Тип отпуска",
                  style: AppFonts.w600s12.copyWith(color: AppColors.greyText),
                ),
                const SizedBox(
                  height: 4,
                ),
                Text(
                  "-",
                  style: AppFonts.w600s16.copyWith(color: AppColors.white),
                ),
                const SizedBox(
                  height: 12,
                ),
                Row(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Начало",
                          style: AppFonts.w600s12
                              .copyWith(color: AppColors.greyText),
                        ),
                        const SizedBox(
                          height: 4,
                        ),
                        Text(
                          "-",
                          style:
                              AppFonts.w600s16.copyWith(color: AppColors.white),
                        ),
                      ],
                    ),
                    const SizedBox(
                      width: 100,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Конец",
                          style: AppFonts.w600s12
                              .copyWith(color: AppColors.greyText),
                        ),
                        const SizedBox(
                          height: 4,
                        ),
                        Text(
                          "-",
                          style:
                              AppFonts.w600s16.copyWith(color: AppColors.white),
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(
                  height: 14,
                ),
                Row(
                  children: [
                    const ImageIcon(
                      AssetImage(AppImages.clock),
                      size: 20,
                      color: AppColors.purple,
                    ),
                    TextButton(
                      onPressed: () {},
                      child: Text(
                        "История",
                        style:
                            AppFonts.w700s14.copyWith(color: AppColors.purple),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
