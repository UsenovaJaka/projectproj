import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:project_x/const/app_colors.dart';
import 'package:project_x/const/app_images.dart';
import 'package:project_x/const/app_style.dart';
import 'package:project_x/modules/profile_page/models/operations_model.dart';

import 'package:project_x/modules/profile_page/ui/widgets/operations_widget.dart';

class TabBarFirst extends StatelessWidget {
  TabBarFirst({
    super.key,
  });
  List<OperationsModel> model = [
    OperationsModel(
        card: '**3456',
        date: '12 марта 2023',
        sum: '100 000',
        title: 'Зарплата за январь'),
    OperationsModel(
        card: '**3456',
        date: '12 марта 2023',
        sum: '100 000',
        title: 'Зарплата за февраль'),
    OperationsModel(
        card: '**3456',
        date: '12 марта 2023',
        sum: '100 000',
        title: 'Зарплата за март'),
    OperationsModel(
        card: '**3456',
        date: '12 марта 2023',
        sum: '100 000',
        title: 'Зарплата за апрель'),
  ];
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          children: [
            Container(
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: AppColors.bg2,
              ),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Наименование банка",
                      style:
                          AppFonts.w600s12.copyWith(color: AppColors.greyText),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        Image.asset(AppImages.logo),
                        SizedBox(width: 6),
                        Text(
                          "Halyk Bank Kyrgyzstan",
                          style:
                              AppFonts.w400s16.copyWith(color: AppColors.white),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Text(
                      "Номер карты или счета",
                      style:
                          AppFonts.w600s12.copyWith(color: AppColors.greyText),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        Text(
                          "1234 5678 9012",
                          style:
                              AppFonts.w400s16.copyWith(color: AppColors.white),
                        ),
                        const Spacer(),
                        IconButton(
                          onPressed: () {
                            Clipboard.setData(
                                ClipboardData(text: "sdfsdfsdfsdf"));
                          },
                          icon: const ImageIcon(
                            AssetImage(AppImages.copy),
                            color: AppColors.cardText,
                          ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Row(
                      children: [
                        Text(
                          "Текущая сумма ЗП",
                          style: AppFonts.w600s12
                              .copyWith(color: AppColors.greyText),
                        ),
                        SizedBox(width: 80),
                        Text(
                          "Валюта",
                          style: AppFonts.w600s12
                              .copyWith(color: AppColors.greyText),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        Text(
                          "100 000 сом",
                          style:
                              AppFonts.w400s16.copyWith(color: AppColors.white),
                        ),
                        const Spacer(),
                        Text(
                          "KGS",
                          style:
                              AppFonts.w400s16.copyWith(color: AppColors.white),
                        ),
                        const Spacer(),
                        const ImageIcon(
                          AssetImage(AppImages.clock),
                          color: AppColors.cardText,
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            Row(
              children: [
                Text(
                  'История начислений',
                  style: AppFonts.w700s18.copyWith(color: AppColors.white),
                ),
                const Spacer(),
                Text('Вся история',
                    style: AppFonts.w700s14.copyWith(color: AppColors.purple))
              ],
            ),
            const SizedBox(
              height: 16,
            ),
            Container(
              padding: const EdgeInsets.only(bottom: 10),
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: AppColors.bg2,
              ),
              child: ListView.separated(
                physics: NeverScrollableScrollPhysics(),
                separatorBuilder: (context, index) {
                  return const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Divider(
                      height: 3,
                      color: AppColors.greyText,
                    ),
                  );
                },
                shrinkWrap: true,
                itemCount: model.length,
                itemBuilder: (BuildContext context, int index) {
                  return OperationsCardWidget(
                    date: model[index].date,
                    card: model[index].card,
                    sum: model[index].sum,
                    title: model[index].title,
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
