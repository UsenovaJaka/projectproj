import 'package:flutter/material.dart';
import 'package:project_x/const/app_colors.dart';
import 'package:project_x/const/app_style.dart';

class Contact_info extends StatelessWidget {
  const Contact_info({
    super.key,
    required this.image,
    required this.info,
  });

  final String image;
  final String info;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Image.asset(image),
        const SizedBox(
          width: 16,
        ),
        InkWell(
          onTap: () {},
          child: Text(
            info,
            style: AppFonts.w400s16.copyWith(
                color: AppColors.white, decoration: TextDecoration.underline),
          ),
        )
      ],
    );
  }
}
