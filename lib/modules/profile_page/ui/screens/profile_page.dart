import 'package:flutter/material.dart';
import 'package:project_x/const/app_colors.dart';
import 'package:project_x/const/app_images.dart';
import 'package:project_x/const/app_style.dart';
import 'package:project_x/modules/employees/ui/widgets/mini_container.dart';
import 'package:project_x/modules/profile_page/ui/widgets/profile_card.dart';
import 'package:project_x/modules/profile_page/ui/widgets/tab_bar_first.dart';
import 'package:project_x/modules/profile_page/ui/widgets/tab_bar_second.dart';
import 'package:project_x/modules/profile_page/ui/widgets/tab_bar_third.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage>
    with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    TabController tabController = TabController(length: 3, vsync: this);

    return Scaffold(
      backgroundColor: AppColors.bg,
      appBar: AppBar(
        backgroundColor: AppColors.bg,
        elevation: 0,
        actions: const [
          Padding(
            padding: EdgeInsets.only(right: 16, top: 16),
            child: MiniContainerWidget(
              image: AssetImage(AppImages.edit),
            ),
          ),
        ],
      ),
      body: NestedScrollView(
        physics: const BouncingScrollPhysics(),
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            const SliverAppBar(
              backgroundColor: AppColors.bg,
              automaticallyImplyLeading: false,
              toolbarHeight: 170,
              title: ProfileCard(),
            ),
            SliverAppBar(
              automaticallyImplyLeading: false,
              pinned: true,
              backgroundColor: AppColors.bg,
              title: TabBar(
                  padding: const EdgeInsets.only(
                    bottom: 20,
                  ),
                  controller: tabController,
                  labelColor: AppColors.purple,
                  unselectedLabelColor: AppColors.greyText,
                  labelStyle: AppFonts.w700s14,
                  indicatorColor: AppColors.purple,
                  tabs: const [
                    Tab(
                      text: 'Начисления',
                    ),
                    Tab(text: 'Контакты'),
                    Tab(
                      text: 'Отпуск',
                    ),
                  ]),
            )
          ];
        },
        body: Container(
          padding: const EdgeInsets.only(bottom: 5),
          width: double.maxFinite,
          child: TabBarView(controller: tabController, children: [
            TabBarFirst(),
            const TabBarSecond(),
            const TabBarThird(),
          ]),
        ),
      ),
    );
  }
}
