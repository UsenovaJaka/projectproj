import 'package:flutter/material.dart';
import 'package:project_x/const/app_colors.dart';
import 'package:project_x/const/app_images.dart';
import 'package:project_x/const/app_style.dart';
import 'package:project_x/modules/employees/ui/widgets/mini_container.dart';

class ProfileDescription extends StatefulWidget {
  ProfileDescription({super.key});

  @override
  State<ProfileDescription> createState() => _ProfileDescriptionState();
}

class _ProfileDescriptionState extends State<ProfileDescription> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.bg,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'О сотруднике',
          style: AppFonts.w600s16.copyWith(color: AppColors.white),
        ),
        backgroundColor: AppColors.bg,
        elevation: 0,
        actions: const [
          Padding(
            padding: EdgeInsets.only(right: 16, top: 16),
            child: MiniContainerWidget(
              image: AssetImage(AppImages.edit),
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 80,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                ),
                child: Column(
                  children: [
                    Row(
                      children: [
                        const CircleAvatar(
                          backgroundImage: AssetImage(AppImages.avatar),
                          radius: 30,
                        ),
                        const SizedBox(
                          width: 16,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Нурдин кызы Бегайым',
                              style: AppFonts.w700s18
                                  .copyWith(color: AppColors.white),
                            ),
                            const SizedBox(
                              height: 4,
                            ),
                            Row(
                              children: [
                                Text(
                                  "UX/UI дизайнер",
                                  style: AppFonts.w400s14
                                      .copyWith(color: AppColors.white),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  '·',
                                  style: AppFonts.w600s14
                                      .copyWith(color: AppColors.white),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  "Intern",
                                  style: AppFonts.w400s14
                                      .copyWith(color: AppColors.white),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "День рождения:",
                        style: AppFonts.w600s12
                            .copyWith(color: AppColors.greyText),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      Text(
                        "17 ноября 2002",
                        style:
                            AppFonts.w400s16.copyWith(color: AppColors.white),
                      ),
                    ],
                  ),
                  const SizedBox(
                    width: 50,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Пол",
                        style: AppFonts.w600s12
                            .copyWith(color: AppColors.greyText),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      Text(
                        "Женский",
                        style:
                            AppFonts.w400s16.copyWith(color: AppColors.white),
                      ),
                    ],
                  )
                ],
              ),
              const SizedBox(
                height: 16,
              ),
              Text(
                "Адрес проживания",
                style: AppFonts.w600s12.copyWith(color: AppColors.greyText),
              ),
              const SizedBox(
                height: 4,
              ),
              Text(
                "Мкр. Восток-5, дом 2/2",
                style: AppFonts.w400s16.copyWith(color: AppColors.white),
              ),
              const SizedBox(
                height: 16,
              ),
              Text(
                "ИНН",
                style: AppFonts.w600s12.copyWith(color: AppColors.greyText),
              ),
              const SizedBox(
                height: 4,
              ),
              Text(
                "11711200250318",
                style: AppFonts.w400s16.copyWith(color: AppColors.white),
              ),
              const SizedBox(
                height: 16,
              ),
              const Divider(
                height: 2,
                color: AppColors.greyText,
              ),
              const SizedBox(
                height: 10,
              ),
              Card(
                margin: EdgeInsets.zero.copyWith(top: 8),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12),
                ),
                clipBehavior: Clip.antiAlias,
                color: AppColors.bg2,
                child: ExpansionTile(
                  collapsedIconColor: AppColors.purple,
                  iconColor: AppColors.purple,
                  title: Row(
                    children: [
                      const ImageIcon(
                        AssetImage(AppImages.home),
                        color: AppColors.purple,
                      ),
                      const SizedBox(
                        width: 7,
                      ),
                      Text(
                        'Родственники',
                        style:
                            AppFonts.w700s14.copyWith(color: AppColors.purple),
                      ),
                    ],
                  ),
                  children: [
                    ListView.separated(
                      physics: const NeverScrollableScrollPhysics(),
                      separatorBuilder: (context, index) {
                        return const Padding(
                          padding: EdgeInsets.symmetric(horizontal: 16),
                          child: Divider(
                            height: 5,
                            color: AppColors.greyText,
                          ),
                        );
                      },
                      shrinkWrap: true,
                      itemCount: 5,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          padding: const EdgeInsets.all(16),
                          color: AppColors.bg2,
                          width: double.infinity,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Отец:',
                                style: AppFonts.w600s14
                                    .copyWith(color: AppColors.white),
                              ),
                              const SizedBox(
                                width: 35,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'ФИО',
                                    style: AppFonts.w600s12
                                        .copyWith(color: AppColors.greyText),
                                  ),
                                  const SizedBox(
                                    height: 4,
                                  ),
                                  Text(
                                    "Пушкин Александр Сергеевич",
                                    style: AppFonts.w400s16
                                        .copyWith(color: AppColors.white),
                                    maxLines: 2,
                                  ),
                                  const SizedBox(
                                    height: 12,
                                  ),
                                  Text(
                                    'Дата рождения',
                                    style: AppFonts.w600s12
                                        .copyWith(color: AppColors.greyText),
                                  ),
                                  const SizedBox(
                                    height: 4,
                                  ),
                                  Text(
                                    "26 августа 1973",
                                    style: AppFonts.w400s16
                                        .copyWith(color: AppColors.white),
                                  ),
                                ],
                              )
                            ],
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
