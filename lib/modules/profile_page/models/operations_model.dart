class OperationsModel {
  final String date;
  final String sum;
  final String card;
  final String title;

  OperationsModel(
      {required this.date,
      required this.sum,
      required this.card,
      required this.title});
}
