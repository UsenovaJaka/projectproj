import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project_x/const/app_colors.dart';
import 'package:project_x/const/app_images.dart';
import 'package:project_x/const/app_style.dart';
import 'package:project_x/injectable/injectable.dart';
import 'package:project_x/modules/employees/core/bloc/employees_bloc.dart';
import 'package:project_x/modules/employees/models/empl_modelOLD.dart';
import 'package:project_x/modules/employees/models/employees_model.dart';
import 'package:project_x/modules/employees/ui/widgets/age_show_button.dart';
import 'package:project_x/modules/employees/ui/widgets/controllers.dart';
import 'package:project_x/modules/employees/ui/widgets/employees_card_widget.dart';
import 'package:project_x/modules/employees/ui/widgets/filter_container.dart';
import 'package:project_x/modules/employees/ui/widgets/from_to_check.dart';
import 'package:project_x/modules/employees/ui/widgets/list_check_widget.dart';
import 'package:project_x/modules/employees/ui/widgets/mini_container.dart';
import 'package:project_x/modules/employees/ui/widgets/sort.dart';

class EmployeesPage extends StatefulWidget {
  EmployeesPage({
    super.key,
  });

  @override
  State<EmployeesPage> createState() => _EmployeesPageState();
}

class _EmployeesPageState extends State<EmployeesPage> {
  List<String> titles = ["Должность", "Зарплата", "Пол", "Возраст", "Статус"];
  FocusNode _focusNode = FocusNode();
  bool _isWidgetVisible = false;
  bool _showClearIcon = false;

  late List<SortModel> modalBottomSheets = [
    SortModel(
      title: 'Должность',
      controller: positionController,
      body: ListCheckWidget(
        controllerOfList: positionController,
      ),
    ),
    SortModel(
      title: 'Зарплата',
      controller: salaryController,
      body: FromToCheckWidget(
        controllerFrom: salaryController.controllerFrom,
        controllerTo: salaryController.controllerTo,
      ),
    ),
    SortModel(
      title: 'Пол',
      controller: genderController,
      body: ListCheckWidget(
        controllerOfList: genderController,
      ),
    ),
    SortModel(
      title: 'Возраст',
      controller: sliderController,
      body: AgeShowButton(
        controller: sliderController,
      ),
    ),
    SortModel(
      title: 'Статус',
      controller: statusController,
      body: ListCheckWidget(
        controllerOfList: statusController,
      ),
    ),
  ];

  Map<String, bool> positions = {
    'Любая': false,
    'UX/UI': false,
    'Flutter': false,
    'Java': false,
    'QA': false,
  };

  // Map<String, bool> grades = {
  //   'Intern': false,
  //   'Junior': false,
  //   'Middle': false,
  //   'Senior': false,
  //   'Lead': false,
  // };

  Map<String, bool> gender = {
    'Мужской': false,
    'Женский': false,
  };
  Map<String, bool> status = {
    'На работе': false,
    'В отпуске': false,
  };

  final controller = TextEditingController();
  late ListController positionController;
  late ListController statusController;
  late ListController genderController;
  late SalaryController salaryController;
  late SliderController sliderController;

  @override
  void initState() {
    super.initState();
    positionController = ListController(false);
    statusController = ListController(false);
    genderController = ListController(false);
    salaryController = SalaryController(false);
    sliderController = SliderController(false);
    positionController.addItems = positions;
    statusController.addItems = status;
    genderController.addItems = gender;
    controller.addListener(searchListener);
  }

  @override
  void dispose() {
    controller.dispose();
    positionController.dispose();
    salaryController.dispose();
    sliderController.dispose();
    super.dispose();
  }

  void searchListener() {
    setState(() {
      _showClearIcon = controller.text.isNotEmpty;
    });
  }

  void _clearText() {
    setState(() {
      controller.clear();
      _showClearIcon = false;
    });
  }

  void _onFocusChange() {
    setState(() {
      _isWidgetVisible = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => sl<EmployeesBloc>()..add(GetEmployees()),
      child: SafeArea(
        child: Scaffold(
          backgroundColor: AppColors.bg,
          body: NestedScrollView(
            floatHeaderSlivers: true,
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverOverlapAbsorber(
                  handle:
                      NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                  sliver: SliverAppBar(
                    backgroundColor: AppColors.bg,
                    floating: true,
                    snap: true,
                    toolbarHeight: (_isWidgetVisible) ? 180 : 150,
                    forceElevated: innerBoxIsScrolled,
                    flexibleSpace: FlexibleSpaceBar(
                      background: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16)
                            .copyWith(top: 38),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 40,
                              child: Row(children: [
                                _isWidgetVisible
                                    ? Padding(
                                        padding:
                                            const EdgeInsets.only(right: 10),
                                        child: InkWell(
                                          onTap: () => setState(() {
                                            _isWidgetVisible = false;
                                            _focusNode.unfocus();
                                          }),
                                          child: ImageIcon(
                                            AssetImage(
                                              AppImages.arrowLeft,
                                            ),
                                            color: AppColors.grey,
                                          ),
                                        ),
                                      )
                                    : SizedBox(),
                                Flexible(
                                  child: TextFormField(
                                    controller: controller,
                                    focusNode: _focusNode
                                      ..addListener(_onFocusChange),
                                    style: const TextStyle(
                                      color: AppColors.white,
                                    ),
                                    decoration: InputDecoration(
                                      contentPadding:
                                          EdgeInsets.all(0).copyWith(left: 10),
                                      fillColor: AppColors.bg2,
                                      filled: true,
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(12),
                                        borderSide: BorderSide.none,
                                      ),
                                      suffixIcon: _showClearIcon
                                          ? _isWidgetVisible
                                              ? Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 10.0),
                                                  child: InkWell(
                                                    child: ImageIcon(
                                                      AssetImage(
                                                          AppImages.close),
                                                      color: AppColors.greyText,
                                                    ),
                                                    onTap: _clearText,
                                                  ),
                                                )
                                              : SizedBox()
                                          : SizedBox(),
                                      hintText: 'Поиск',
                                      hintStyle: AppFonts.w600s16
                                          .copyWith(color: AppColors.greyText),
                                      prefixIcon: (_isWidgetVisible)
                                          ? null
                                          : const ImageIcon(
                                              AssetImage(AppImages.search),
                                              size: 24,
                                              color: AppColors.cardText,
                                            ),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  width: 12,
                                ),
                                _isWidgetVisible
                                    ? MiniContainerWidget(
                                        image: AssetImage(AppImages.sort),
                                        onPressed: () {},
                                      )
                                    : const MiniContainerWidget(
                                        image:
                                            AssetImage(AppImages.notification),
                                      ),
                              ]),
                            ),
                            SizedBox(
                              height: 22,
                            ),
                            (_isWidgetVisible)
                                ? Expanded(
                                    child: ListView.separated(
                                      separatorBuilder: (context, index) {
                                        return const SizedBox(width: 16);
                                      },
                                      physics: ClampingScrollPhysics(),
                                      shrinkWrap: true,
                                      scrollDirection: Axis.horizontal,
                                      itemCount: titles.length,
                                      itemBuilder:
                                          (BuildContext context, int index) =>
                                              FilterContainer(
                                        title: titles[index],
                                        onTab: () async {
                                          await CustomShowModalSheet
                                              .showBottomSheet(
                                            context,
                                            body: modalBottomSheets[index].body,
                                            title:
                                                modalBottomSheets[index].title,
                                            controller: modalBottomSheets[index]
                                                .controller,
                                          );
                                        },
                                      ),
                                    ),
                                  )
                                : SizedBox(),
                            (_isWidgetVisible)
                                ? SizedBox(
                                    height: 22,
                                  )
                                : SizedBox(
                                    height: 10,
                                  ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Flexible(
                                  child: Text(
                                    'Всего сотрудников: 14',
                                    style: AppFonts.w700s18
                                        .copyWith(color: AppColors.white),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {},
                                  child: Row(
                                    children: [
                                      const ImageIcon(
                                        AssetImage(AppImages.add),
                                        size: 24,
                                        color: AppColors.purple,
                                      ),
                                      const SizedBox(
                                        width: 5,
                                      ),
                                      Text(
                                        "Добавить",
                                        style: AppFonts.w700s16
                                            .copyWith(color: AppColors.purple),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ];
            },
            body: BlocBuilder<EmployeesBloc, EmployeesState>(
              builder: (context, state) {
                if (state is EmployeesLoading) {
                  return CircularProgressIndicator.adaptive();
                }
                if (state is EmployeesError) {
                  return Text('data');
                }
                if (state is EmployeesSucces) {
                  final model = state.model;
                  return ListView.separated(
                      keyboardDismissBehavior:
                          ScrollViewKeyboardDismissBehavior.onDrag,
                      padding: const EdgeInsets.symmetric(horizontal: 16)
                          .copyWith(top: 10),
                      separatorBuilder: (context, index) {
                        return const SizedBox(height: 16);
                      },
                      itemCount: 20,
                      itemBuilder: (BuildContext context, int index) =>
                          EmployeeCardWidget(
                            model: model.data?[index],
                            isFree: false,
                          ));
                }

                return Text(
                  'erorrororor',
                  style: AppFonts.w400s14.copyWith(color: AppColors.white),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
