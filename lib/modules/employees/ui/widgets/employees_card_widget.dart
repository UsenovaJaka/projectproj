import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:project_x/const/app_colors.dart';
import 'package:project_x/const/app_images.dart';
import 'package:project_x/const/app_style.dart';
import 'package:project_x/modules/employees/models/empl_modelOLD.dart';
import 'package:project_x/modules/employees/models/employees_model.dart';
import 'package:project_x/routess/routess.gr.dart';

class EmployeeCardWidget extends StatelessWidget {
  const EmployeeCardWidget({
    super.key,
    this.model,
    required this.isFree,
  });

  final Data? model;
  final bool isFree;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(12),
      onTap: () => context.router.push(ProfilePageRoute()),
      child: Ink(
        width: double.infinity,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12), color: AppColors.bg2),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16)
                  .copyWith(top: 2, bottom: 5),
              child: Row(
                children: [
                  const CircleAvatar(
                    backgroundImage: AssetImage(AppImages.avatar),
                    radius: 30,
                  ),
                  const SizedBox(
                    width: 16,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(
                          height: 10,
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text(
                            maxLines: 1,
                            softWrap: false,
                            overflow: TextOverflow.ellipsis,
                            model?.surname ?? 'surname',
                            style: AppFonts.w700s16
                                .copyWith(color: AppColors.white),
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Flexible(
                              flex: 2,
                              child: Text(
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                model?.position ?? 'position',
                                style: AppFonts.w600s14
                                    .copyWith(color: AppColors.cardText),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 8),
                              child: Text(
                                '·',
                                style: AppFonts.w600s14
                                    .copyWith(color: AppColors.white),
                              ),
                            ),
                            Flexible(
                              child: Text(
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                model?.position ?? 'position',
                                style: AppFonts.w600s14
                                    .copyWith(color: AppColors.cardText),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Text(
                          model?.contacts?.contactNumber ?? 'contacts',
                          style:
                              AppFonts.w400s14.copyWith(color: AppColors.white),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            isFree
                ? Align(
                    alignment: Alignment.bottomLeft,
                    child: DecoratedBox(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 2),
                        child: Text(
                          'В отпуске',
                          style:
                              AppFonts.w700s10.copyWith(color: AppColors.white),
                        ),
                      ),
                      decoration: BoxDecoration(
                        color: AppColors.purple,
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(12),
                          topRight: Radius.circular(12),
                        ),
                      ),
                    ),
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }
}
