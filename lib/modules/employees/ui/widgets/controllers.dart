import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class CustomController<T> extends ChangeNotifier implements ValueListenable<T> {
  T _value;

  CustomController(this._value);

  void clear() {
    notifyListeners();
  }

  @override
  T get value => _value;
  set value(T newValue) {
    _value = newValue;
    notifyListeners();
  }
}

class ListController extends CustomController {
  Map<String, bool>? _items;

  ListController(super._value) {}

  Map<String, bool>? get items => _items;

  void set addItems(Map<String, bool> items) {
    this._items = items;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void clear() {
    List<String>? keyss = _items?.keys.toList();
    for (var element in keyss!) {
      items![element] = false;
    }
    isChecked();
    super.clear();
  }

  void updateCheckbox(int index) {
    if (_items != null) {
      List<String> keyss = _items!.keys.toList();
      final key = keyss[index];
      items![key] = !items![key]!;
      isChecked();
      notifyListeners();
    }
  }

  void isChecked() {
    this.value =
        !(_items?.entries.every((element) => element.value == false) ?? true);
  }
}

class SalaryController extends CustomController {
  late final TextEditingController controller;
  late final TextEditingController controllerFrom;
  late final TextEditingController controllerTo;
  SalaryController(super._value) {
    controller = TextEditingController();
    controllerFrom = TextEditingController();
    controllerTo = TextEditingController();
  }

  @override
  void dispose() {
    controller.dispose();
    controllerFrom.dispose();
    controllerTo.dispose();
    super.dispose();
  }

  @override
  void clear() {
    controllerFrom.clear();
    controllerTo.clear();

    super.clear();
  }

  isChecked() {
    //реализовать
    {}
  }
}

class SliderController extends CustomController {
  late RangeValues _current;
  final RangeValues kDefault = const RangeValues(18, 60);

  SliderController(super._value) {
    _current = RangeValues(18, 60);
  }

  RangeValues get current => _current;

  void set updateRange(RangeValues value) {
    _current = value;
    _isChecked();
    notifyListeners();
  }

  int getStart() => _current.start.round();

  int getEnd() => _current.end.round();

  bool isDefault() {
    return _current == kDefault;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void clear() {
    _current = kDefault;
    _isChecked();
    super.clear();
  }

  void _isChecked() {
    this.value = _current != kDefault;
  }
}
