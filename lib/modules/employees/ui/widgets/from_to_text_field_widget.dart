import 'package:flutter/material.dart';
import 'package:project_x/const/app_colors.dart';
import 'package:project_x/const/app_style.dart';

class FromToTextField extends StatefulWidget {
  final String hintText;
  final TextEditingController controller;

  const FromToTextField(
      {Key? key, required this.hintText, required this.controller})
      : super(key: key);

  @override
  _FromToTextFieldState createState() => _FromToTextFieldState();
}

class _FromToTextFieldState extends State<FromToTextField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 48,
      child: TextFormField(
        keyboardType: TextInputType.number,
        autofocus: true,
        controller: widget.controller,
        style: AppFonts.w600s16.copyWith(color: AppColors.white),
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(10),
          filled: true,
          fillColor: AppColors.bg2,
          hintText: widget.hintText,
          hintStyle: AppFonts.w200s16.copyWith(color: AppColors.greyText),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(12),
              borderSide: BorderSide(color: AppColors.bg2)),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(12),
              borderSide: BorderSide(color: AppColors.purple)),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(12),
              borderSide: BorderSide(color: AppColors.bg2)),
        ),
      ),
    );
  }
}
