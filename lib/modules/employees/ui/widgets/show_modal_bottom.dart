import 'package:flutter/material.dart';
import 'package:project_x/const/app_colors.dart';

class ShowModalBottomCubClass {
  static ShowButtonContent(BuildContext context, Widget content) {
    showModalBottomSheet(
      isScrollControlled: true,
      barrierColor: AppColors.black.withOpacity(0.8),
      backgroundColor: AppColors.bg,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16),
          topRight: Radius.circular(16),
        ),
      ),
      builder: ((context) {
        return Container(
          padding: MediaQuery.of(context).viewInsets,
          decoration: BoxDecoration(
            color: AppColors.bg,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16),
              topRight: Radius.circular(16),
            ),
          ),
          child: content,
        );
      }),
    );
  }
}
