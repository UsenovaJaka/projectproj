import 'package:flutter/material.dart';
import 'package:project_x/const/app_colors.dart';
import 'package:project_x/const/app_style.dart';
import 'package:project_x/modules/employees/ui/widgets/controllers.dart';

class AgeShowButton extends StatefulWidget {
  AgeShowButton({
    super.key,
    required this.controller,
  });

  final SliderController controller;

  @override
  State<AgeShowButton> createState() => _AgeShowButtonState();
}

class _AgeShowButtonState extends State<AgeShowButton> {
  @override
  void initState() {
    widget.controller.addListener(update);
    super.initState();
  }

  @override
  void dispose() {
    widget.controller.removeListener(update);
    super.dispose();
  }

  void update() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    int start = widget.controller.getStart();
    int end = widget.controller.getEnd();
    return Row(
      children: [
        Expanded(
          child: RangeSlider(
            activeColor: AppColors.purple,
            inactiveColor: AppColors.bg2,
            values: widget.controller.current,
            min: 18,
            max: 60,
            onChanged: (RangeValues values) {
              widget.controller.updateRange = values;
            },
          ),
        ),
        Text(
          widget.controller.isDefault() ? 'Любой' : '$start-$end',
          style: AppFonts.w600s16.copyWith(color: AppColors.showBarBorder),
        ),
      ],
    );
  }
}
