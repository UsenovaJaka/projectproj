import 'package:flutter/material.dart';
import 'package:project_x/modules/employees/ui/widgets/from_to_text_field_widget.dart';
import 'package:project_x/modules/employees/ui/widgets/radio_custom.dart';

class FromToCheckWidget extends StatefulWidget {
  FromToCheckWidget({
    super.key,
    required this.controllerFrom,
    required this.controllerTo,
  });
  final TextEditingController controllerFrom;
  final TextEditingController controllerTo;

  @override
  State<FromToCheckWidget> createState() => _FromToCheckWidgetState();
}

class _FromToCheckWidgetState extends State<FromToCheckWidget> {
  int selectedRadio = 1;

  @override
  void initState() {
    super.initState();
  }

  setSelectedRadio(int val) {
    setState(() {
      selectedRadio = val;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Flexible(
              child: FromToTextField(
                hintText: 'От',
                controller: widget.controllerFrom,
              ),
            ),
            SizedBox(
              width: 16,
            ),
            Flexible(
              child: FromToTextField(
                hintText: 'До',
                controller: widget.controllerTo,
              ),
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Row(
          children: [
            CustomRadioButton(
              onChanged: (val) {
                setSelectedRadio(val!);
              },
              groupValue: selectedRadio,
              title: 'сом',
              value: 1,
            ),
            CustomRadioButton(
              onChanged: (val) {
                setSelectedRadio(val!);
              },
              groupValue: selectedRadio,
              title: 'доллары',
              value: 2,
            ),
          ],
        ),
        SizedBox(
          height: 35,
        ),
      ],
    );
  }
}
