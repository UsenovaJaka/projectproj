import 'package:flutter/material.dart';
import 'package:project_x/const/app_colors.dart';

class MiniContainerWidget extends StatelessWidget {
  const MiniContainerWidget({
    super.key,
    required this.image,
    this.onPressed,
  });

  final ImageProvider image;
  final Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 40,
      width: 40,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
            padding: EdgeInsets.zero,
            backgroundColor: AppColors.bg2,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            )),
        onPressed: () {},
        child: ImageIcon(
          image,
          color: AppColors.cardText,
        ),
      ),
    );
  }
}
