import 'package:flutter/material.dart';
import 'package:project_x/const/app_colors.dart';
import 'package:project_x/const/app_images.dart';
import 'package:project_x/const/app_style.dart';

class FilterContainer extends StatelessWidget {
  const FilterContainer({
    super.key,
    required this.title,
    required this.onTab,
  });

  final String title;
  final Function() onTab;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTab,
      child: Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          decoration: BoxDecoration(
            color: AppColors.bg,
            borderRadius: BorderRadius.circular(16),
            border: Border.all(color: AppColors.showBarBorder, width: 1),
          ),
          child: Row(
            children: [
              Text(
                title,
                style: AppFonts.w700s14.copyWith(color: AppColors.cardText),
              ),
              SizedBox(
                width: 3,
              ),
              ImageIcon(
                  AssetImage(
                    AppImages.arrowDown,
                  ),
                  color: AppColors.cardText)
            ],
          )),
    );
  }
}
