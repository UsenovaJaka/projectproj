import 'package:flutter/material.dart';
import 'package:project_x/const/app_colors.dart';
import 'package:project_x/const/app_images.dart';
import 'package:project_x/const/app_style.dart';
import 'package:project_x/modules/employees/ui/widgets/controllers.dart';
import 'package:project_x/modules/employees/ui/widgets/show_modal_bottom.dart';
import 'package:project_x/modules/login_page/ui/widgets/login_button.dart';

class SortModel {
  final String title;
  final Widget body;
  final CustomController? controller;
  SortModel({
    this.controller,
    required this.title,
    required this.body,
  });
}

class CustomShowModalSheet {
  static Future<void> showBottomSheet(
    BuildContext context, {
    required String title,
    required Widget body,
    CustomController? controller,
  }) async {
    await ShowModalBottomCubClass.ShowButtonContent(
      context,
      Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const ImageIcon(
            AssetImage(AppImages.homeIndicator),
            color: AppColors.showBarBorder,
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 16).copyWith(bottom: 40),
            child: Stack(
              children: [
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 16, bottom: 30),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            title,
                            style: AppFonts.w600s16
                                .copyWith(color: AppColors.grey),
                          ),
                        ],
                      ),
                    ),
                    body,
                    SizedBox(
                      width: double.infinity,
                      child: AppElevatedButton(
                        onPressed: () {},
                        title: 'Показать результаты',
                      ),
                    ),
                  ],
                ),
                if (controller != null)
                  ValueListenableBuilder(
                    valueListenable: controller,
                    builder: (context, value, child) {
                      print(value);
                      return Positioned(
                        right: 0,
                        top: 16,
                        child: GestureDetector(
                          onTap: () {
                            controller.clear();
                          },
                          child: Text(
                            'Сбросить',
                            style: AppFonts.w600s16.copyWith(
                              color: value == true
                                  ? AppColors.purple
                                  : AppColors.showBarBorder,
                            ),
                          ),
                        ),
                      );
                    },
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
