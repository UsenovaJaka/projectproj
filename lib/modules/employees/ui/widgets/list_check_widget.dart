import 'package:flutter/material.dart';
import 'package:project_x/const/app_colors.dart';
import 'package:project_x/const/app_style.dart';
import 'package:project_x/modules/employees/ui/widgets/controllers.dart';

class ListCheckWidget extends StatefulWidget {
  const ListCheckWidget({
    super.key,
    this.controllerOfList,
  });
  final ListController? controllerOfList;
  @override
  State<ListCheckWidget> createState() => _ListCheckWidgetState();
}

class _ListCheckWidgetState extends State<ListCheckWidget> {
  @override
  void initState() {
    widget.controllerOfList
        ?.addListener(() => mounted ? setState(() {}) : null);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    widget.controllerOfList?.items?.entries.forEach((e) {});

    return ListView.builder(
      padding: EdgeInsets.only(bottom: 30),
      shrinkWrap: true,
      itemCount: widget.controllerOfList?.items?.length,
      itemBuilder: (context, index) {
        return InkWell(
          onTap: () => widget.controllerOfList?.updateCheckbox(index),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  widget.controllerOfList?.items?.entries
                          .elementAt(index)
                          .key ??
                      '',
                  style: AppFonts.w400s16.copyWith(color: AppColors.white),
                ),
                Container(
                  height: 18,
                  width: 18,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6.5),
                    color: AppColors.showBarBorder,
                  ),
                  child: Checkbox(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(6.5),
                    ),
                    fillColor: MaterialStateProperty.resolveWith(
                      (states) {
                        return states.contains(MaterialState.selected)
                            ? AppColors.purple
                            : AppColors.showBarBorder;
                      },
                    ),
                    value: widget.controllerOfList?.items?.entries
                            .elementAt(index)
                            .value ??
                        false,
                    onChanged: (bool? value) {
                      widget.controllerOfList?.updateCheckbox(index);
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
