import 'package:flutter/material.dart';
import 'package:project_x/const/app_colors.dart';
import 'package:project_x/const/app_style.dart';

class CustomRadioButton<T> extends StatelessWidget {
  const CustomRadioButton({
    Key? key,
    required this.groupValue,
    required this.value,
    required this.title,
    this.onChanged,
  }) : super(key: key);

  final T? groupValue;

  final T value;
  final String title;
  final ValueChanged<T?>? onChanged;

  @override
  Widget build(BuildContext context) {
    return MenuItemButton(
      onPressed: onChanged == null
          ? null
          : () {
              onChanged!.call(value);
            },
      leadingIcon: ConstrainedBox(
        constraints: const BoxConstraints(
          maxHeight: Checkbox.width,
          maxWidth: Checkbox.width,
        ),
        child: Radio(
          activeColor: AppColors.purple,
          fillColor: MaterialStateProperty.resolveWith((states) {
            return states.contains(MaterialState.selected)
                ? AppColors.purple
                : AppColors.showBarBorder;
          }),
          value: value,
          groupValue: groupValue,
          onChanged: onChanged,
        ),
      ),
      child:
          Text(title, style: AppFonts.w600s16.copyWith(color: AppColors.white)),
    );
  }
}
