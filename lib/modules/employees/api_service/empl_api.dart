import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:project_x/modules/employees/models/employees_model.dart';
import 'package:retrofit/retrofit.dart';

part 'empl_api.g.dart';

@lazySingleton
@RestApi()
abstract class ApiService {
  @factoryMethod
  factory ApiService(Dio dio) = _ApiService;

  @GET('/get-employees')
  Future<EmployeeModel> getEmployees();

  // Future<List<EmployeesModel>> searchEmployees(
  //     String name, String position) async {
  //   try {
  //     final queryParameters = {
  //       'name': name,
  //       'position': position,
  //     };
  //     final response =
  //         await get('/search/employees', queryParameters: queryParameters);
  //     final data = response.data as List<dynamic>;
  //     final employees =
  //         data.map((item) => EmployeesModel.fromJson(item)).toList();
  //     return employees;
  //   } catch (e) {
  //     throw Exception('error: $e');
  //   }
  // }
}
