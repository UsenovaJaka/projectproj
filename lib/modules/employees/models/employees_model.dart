// import 'package:json_annotation/json_annotation.dart';

// part 'employees_model.g.dart';

// @JsonSerializable(explicitToJson: true)
// class EmployeesModel {
//   final int? id;
//   final String? birthDate;
//   final String? surname;
//   final String? name;
//   final String? patronymic;
//   final String? position;
//   final String? grade;
//   final String? contacts;
//   final double? currentSalary;
//   final String? inn;
//   final String? photo;
//   final String? address;
//   final String? gender;
//   final String? positionLastUpdate;
//   final String? interviewId;
//   final int? profileFillPercentage;

//   EmployeesModel({
//     this.id,
//     this.birthDate,
//     this.surname,
//     this.name,
//     this.patronymic,
//     this.position,
//     this.grade,
//     this.contacts,
//     this.currentSalary,
//     this.inn,
//     this.photo,
//     this.address,
//     this.gender,
//     this.positionLastUpdate,
//     this.interviewId,
//     this.profileFillPercentage,
//   });

//   factory EmployeesModel.fromJson(Map<String, dynamic> json) =>
//       _$EmployeesModelFromJson(json);

//   Map<String, dynamic> toJson() => _$EmployeesModelToJson(this);
// }

import 'package:json_annotation/json_annotation.dart';
part 'employees_model.g.dart';

@JsonSerializable()
class EmployeeModel {
  String? status;
  List<Data>? data;
  String? message;

  EmployeeModel({
    this.status,
    this.data,
    this.message,
  });

  factory EmployeeModel.fromJson(Map<String, dynamic> json) =>
      _$EmployeeModelFromJson(json);
}

@JsonSerializable()
class Data {
  int? id;
  String? birthDate;
  String? surname;
  String? name;
  String? patronymic;
  String? position;
  String? grade;
  Contacts? contacts;
  String? currentSalary;
  String? inn;
  String? photo;
  String? address;
  String? gender;
  String? positionLastUpdate;
  int? interviewId;
  int? profileFillPercentage;

  Data({
    this.id,
    this.birthDate,
    this.surname,
    this.name,
    this.patronymic,
    this.position,
    this.grade,
    this.contacts,
    this.currentSalary,
    this.inn,
    this.photo,
    this.address,
    this.gender,
    this.positionLastUpdate,
    this.interviewId,
    this.profileFillPercentage,
  });

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);
}

@JsonSerializable()
class Contacts {
  int? id;
  String? contactNumber;
  String? email;
  String? whatsAppNumber;
  String? telegramNumber;
  String? linkedInAccount;

  Contacts({
    this.id,
    this.contactNumber,
    this.email,
    this.whatsAppNumber,
    this.telegramNumber,
    this.linkedInAccount,
  });

  factory Contacts.fromJson(Map<String, dynamic> json) =>
      _$ContactsFromJson(json);
}
