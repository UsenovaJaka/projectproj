// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'employees_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EmployeeModel _$EmployeeModelFromJson(Map<String, dynamic> json) =>
    EmployeeModel(
      status: json['status'] as String?,
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => Data.fromJson(e as Map<String, dynamic>))
          .toList(),
      message: json['message'] as String?,
    );

Map<String, dynamic> _$EmployeeModelToJson(EmployeeModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'data': instance.data,
      'message': instance.message,
    };

Data _$DataFromJson(Map<String, dynamic> json) => Data(
      id: json['id'] as int?,
      birthDate: json['birthDate'] as String?,
      surname: json['surname'] as String?,
      name: json['name'] as String?,
      patronymic: json['patronymic'] as String?,
      position: json['position'] as String?,
      grade: json['grade'] as String?,
      contacts: json['contacts'] == null
          ? null
          : Contacts.fromJson(json['contacts'] as Map<String, dynamic>),
      currentSalary: json['currentSalary'] as String?,
      inn: json['inn'] as String?,
      photo: json['photo'] as String?,
      address: json['address'] as String?,
      gender: json['gender'] as String?,
      positionLastUpdate: json['positionLastUpdate'] as String?,
      interviewId: json['interviewId'] as int?,
      profileFillPercentage: json['profileFillPercentage'] as int?,
    );

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'id': instance.id,
      'birthDate': instance.birthDate,
      'surname': instance.surname,
      'name': instance.name,
      'patronymic': instance.patronymic,
      'position': instance.position,
      'grade': instance.grade,
      'contacts': instance.contacts,
      'currentSalary': instance.currentSalary,
      'inn': instance.inn,
      'photo': instance.photo,
      'address': instance.address,
      'gender': instance.gender,
      'positionLastUpdate': instance.positionLastUpdate,
      'interviewId': instance.interviewId,
      'profileFillPercentage': instance.profileFillPercentage,
    };

Contacts _$ContactsFromJson(Map<String, dynamic> json) => Contacts(
      id: json['id'] as int?,
      contactNumber: json['contactNumber'] as String?,
      email: json['email'] as String?,
      whatsAppNumber: json['whatsAppNumber'] as String?,
      telegramNumber: json['telegramNumber'] as String?,
      linkedInAccount: json['linkedInAccount'] as String?,
    );

Map<String, dynamic> _$ContactsToJson(Contacts instance) => <String, dynamic>{
      'id': instance.id,
      'contactNumber': instance.contactNumber,
      'email': instance.email,
      'whatsAppNumber': instance.whatsAppNumber,
      'telegramNumber': instance.telegramNumber,
      'linkedInAccount': instance.linkedInAccount,
    };
