import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:project_x/modules/employees/api_service/empl_api.dart';
import 'package:project_x/modules/employees/models/employees_model.dart';

@lazySingleton
class EmployeesRepository {
  final ApiService apiService;

  EmployeesRepository(this.apiService);

  Future<EmployeeModel> getEmployees() async {
    try {
      return await apiService.getEmployees();
    } catch (error) {
      rethrow;
    }
  }
}
