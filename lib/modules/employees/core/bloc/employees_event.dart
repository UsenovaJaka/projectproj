part of 'employees_bloc.dart';

@immutable
abstract class EmployeesEvent {}

class GetEmployees extends EmployeesEvent {}

class SearchEmployees extends EmployeesEvent {}
