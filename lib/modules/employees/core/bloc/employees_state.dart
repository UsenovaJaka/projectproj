part of 'employees_bloc.dart';

@immutable
abstract class EmployeesState {}

class EmployeesInitial extends EmployeesState {}

class EmployeesLoading extends EmployeesState {}

class EmployeesSucces extends EmployeesState {
  final EmployeeModel model;
  EmployeesSucces({required this.model});
}

class EmployeesError extends EmployeesState {}
