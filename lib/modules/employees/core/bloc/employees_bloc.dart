import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:injectable/injectable.dart';
import 'package:project_x/modules/employees/core/repo/repo_emp.dart';
import 'package:project_x/modules/employees/models/employees_model.dart';

part 'employees_event.dart';
part 'employees_state.dart';

@injectable
class EmployeesBloc extends Bloc<EmployeesEvent, EmployeesState> {
  EmployeesBloc(this._employeesRepository) : super(EmployeesInitial()) {
    on<GetEmployees>(getEmployees);
//  on<SearchEmployees>(searchEmployees);
  }

  final EmployeesRepository _employeesRepository;

  getEmployees(event, emit) async {
    try {
      emit(EmployeesLoading());
      final model = await _employeesRepository.getEmployees();
      emit(EmployeesSucces(model: model));
    } catch (e) {
      emit(EmployeesError());
      print('jjj*************************$e');
    }
  }
// searchEmployees(String name, String position) async {
//     emit(EmployeesLoading());
//     try {
//       final employees = await repository.searchEmployees(name, position);
//       emit(EmployeesLoading());
//     } catch (e) {
//       emit(EmployeesError(errorMessage: "er"));
//     }
//   }
}
