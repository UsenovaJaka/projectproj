import 'package:flutter/material.dart';

abstract class AppColors {
  static const Color white = Colors.white;
  static const Color black = Colors.black;
  static const Color bg = Color(0xff08101D);
  static const Color bg2 = Color(0xff14182B);
  static const Color purple = Color(0xff7F79FF);
  static const Color greyText = Color(0xff888995);
  static const Color cardText = Color(0xffC7C7C7);
  static const Color showBarBorder = Color(0xff3C4157);
  static const Color red = Color(0xffC14040);
  static const Color grey = Color(0xffDDDDDD);
}
