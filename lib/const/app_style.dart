import 'package:flutter/cupertino.dart';

abstract class AppFonts {
  static const TextStyle w600s18 = TextStyle(
      fontSize: 18, fontWeight: FontWeight.w600, fontFamily: 'NunitoSans');
  static const TextStyle w700s24 = TextStyle(
      fontSize: 24, fontWeight: FontWeight.w700, fontFamily: 'NunitoSans');
  static const TextStyle w700s20 = TextStyle(
      fontSize: 20, fontWeight: FontWeight.w700, fontFamily: 'NunitoSans');
  static const TextStyle w700s12 = TextStyle(
      fontSize: 12, fontWeight: FontWeight.w700, fontFamily: 'NunitoSans');
  static const TextStyle w700s16 = TextStyle(
      fontSize: 16, fontWeight: FontWeight.w700, fontFamily: 'NunitoSans');
  static const TextStyle w700s18 = TextStyle(
      fontSize: 18, fontWeight: FontWeight.w700, fontFamily: 'NunitoSans');
  static const TextStyle w600s14 = TextStyle(
      fontSize: 14, fontWeight: FontWeight.w600, fontFamily: 'NunitoSans');
  static const TextStyle w400s14 = TextStyle(
      fontSize: 14, fontWeight: FontWeight.w400, fontFamily: 'NunitoSans');
  static const TextStyle w600s16 = TextStyle(
      fontSize: 16, fontWeight: FontWeight.w600, fontFamily: 'NunitoSans');
  static const TextStyle w400s16 = TextStyle(
      fontSize: 16, fontWeight: FontWeight.w400, fontFamily: 'NunitoSans');
  static const TextStyle w700s14 = TextStyle(
      fontSize: 14, fontWeight: FontWeight.w700, fontFamily: 'NunitoSans');
  static const TextStyle w600s12 = TextStyle(
      fontSize: 12, fontWeight: FontWeight.w600, fontFamily: 'NunitoSans');
  static const TextStyle w500s16 = TextStyle(
      fontSize: 16, fontWeight: FontWeight.w500, fontFamily: 'NunitoSans');
  static const TextStyle w700s10 = TextStyle(
      fontSize: 10, fontWeight: FontWeight.w700, fontFamily: 'NunitoSans');
  static const TextStyle w200s16 = TextStyle(
      fontSize: 16, fontWeight: FontWeight.w200, fontFamily: 'NunitoSans');
}
