// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i7;
import 'package:flutter/material.dart' as _i8;

import '../modules/employees/ui/screens/employees.dart' as _i6;
import '../modules/employees/ui/widgets/page_of_filter.dart' as _i5;
import '../modules/home/ui/screens/home_page.dart' as _i2;
import '../modules/login_page/ui/screens/login_page.dart' as _i1;
import '../modules/profile_page/ui/screens/profile_description.dart' as _i4;
import '../modules/profile_page/ui/screens/profile_page.dart' as _i3;

class AppRouter extends _i7.RootStackRouter {
  AppRouter([_i8.GlobalKey<_i8.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i7.PageFactory> pagesMap = {
    LoginPageRoute.name: (routeData) {
      final args = routeData.argsAs<LoginPageRouteArgs>(
          orElse: () => const LoginPageRouteArgs());
      return _i7.MaterialPageX<dynamic>(
        routeData: routeData,
        child: _i1.LoginPage(key: args.key),
      );
    },
    HomePageRoute.name: (routeData) {
      final args = routeData.argsAs<HomePageRouteArgs>(
          orElse: () => const HomePageRouteArgs());
      return _i7.MaterialPageX<dynamic>(
        routeData: routeData,
        child: _i2.HomePage(key: args.key),
      );
    },
    ProfilePageRoute.name: (routeData) {
      return _i7.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i3.ProfilePage(),
      );
    },
    ProfileDescriptionRoute.name: (routeData) {
      final args = routeData.argsAs<ProfileDescriptionRouteArgs>(
          orElse: () => const ProfileDescriptionRouteArgs());
      return _i7.MaterialPageX<dynamic>(
        routeData: routeData,
        child: _i4.ProfileDescription(key: args.key),
      );
    },
    PageFilteringRoute.name: (routeData) {
      return _i7.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i5.PageFiltering(),
      );
    },
    EmployeesPageRoute.name: (routeData) {
      final args = routeData.argsAs<EmployeesPageRouteArgs>(
          orElse: () => const EmployeesPageRouteArgs());
      return _i7.MaterialPageX<dynamic>(
        routeData: routeData,
        child: _i6.EmployeesPage(key: args.key),
      );
    },
  };

  @override
  List<_i7.RouteConfig> get routes => [
        _i7.RouteConfig(
          LoginPageRoute.name,
          path: '/',
        ),
        _i7.RouteConfig(
          HomePageRoute.name,
          path: '/home',
          children: [
            _i7.RouteConfig(
              EmployeesPageRoute.name,
              path: 'home',
              parent: HomePageRoute.name,
            )
          ],
        ),
        _i7.RouteConfig(
          ProfilePageRoute.name,
          path: '/profile',
        ),
        _i7.RouteConfig(
          ProfileDescriptionRoute.name,
          path: '/description',
        ),
        _i7.RouteConfig(
          PageFilteringRoute.name,
          path: '/filter',
        ),
      ];
}

/// generated route for
/// [_i1.LoginPage]
class LoginPageRoute extends _i7.PageRouteInfo<LoginPageRouteArgs> {
  LoginPageRoute({_i8.Key? key})
      : super(
          LoginPageRoute.name,
          path: '/',
          args: LoginPageRouteArgs(key: key),
        );

  static const String name = 'LoginPageRoute';
}

class LoginPageRouteArgs {
  const LoginPageRouteArgs({this.key});

  final _i8.Key? key;

  @override
  String toString() {
    return 'LoginPageRouteArgs{key: $key}';
  }
}

/// generated route for
/// [_i2.HomePage]
class HomePageRoute extends _i7.PageRouteInfo<HomePageRouteArgs> {
  HomePageRoute({
    _i8.Key? key,
    List<_i7.PageRouteInfo>? children,
  }) : super(
          HomePageRoute.name,
          path: '/home',
          args: HomePageRouteArgs(key: key),
          initialChildren: children,
        );

  static const String name = 'HomePageRoute';
}

class HomePageRouteArgs {
  const HomePageRouteArgs({this.key});

  final _i8.Key? key;

  @override
  String toString() {
    return 'HomePageRouteArgs{key: $key}';
  }
}

/// generated route for
/// [_i3.ProfilePage]
class ProfilePageRoute extends _i7.PageRouteInfo<void> {
  const ProfilePageRoute()
      : super(
          ProfilePageRoute.name,
          path: '/profile',
        );

  static const String name = 'ProfilePageRoute';
}

/// generated route for
/// [_i4.ProfileDescription]
class ProfileDescriptionRoute
    extends _i7.PageRouteInfo<ProfileDescriptionRouteArgs> {
  ProfileDescriptionRoute({_i8.Key? key})
      : super(
          ProfileDescriptionRoute.name,
          path: '/description',
          args: ProfileDescriptionRouteArgs(key: key),
        );

  static const String name = 'ProfileDescriptionRoute';
}

class ProfileDescriptionRouteArgs {
  const ProfileDescriptionRouteArgs({this.key});

  final _i8.Key? key;

  @override
  String toString() {
    return 'ProfileDescriptionRouteArgs{key: $key}';
  }
}

/// generated route for
/// [_i5.PageFiltering]
class PageFilteringRoute extends _i7.PageRouteInfo<void> {
  const PageFilteringRoute()
      : super(
          PageFilteringRoute.name,
          path: '/filter',
        );

  static const String name = 'PageFilteringRoute';
}

/// generated route for
/// [_i6.EmployeesPage]
class EmployeesPageRoute extends _i7.PageRouteInfo<EmployeesPageRouteArgs> {
  EmployeesPageRoute({_i8.Key? key})
      : super(
          EmployeesPageRoute.name,
          path: 'home',
          args: EmployeesPageRouteArgs(key: key),
        );

  static const String name = 'EmployeesPageRoute';
}

class EmployeesPageRouteArgs {
  const EmployeesPageRouteArgs({this.key});

  final _i8.Key? key;

  @override
  String toString() {
    return 'EmployeesPageRouteArgs{key: $key}';
  }
}
