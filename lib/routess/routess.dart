import 'package:auto_route/auto_route.dart';
import 'package:project_x/modules/employees/ui/screens/employees.dart';
import 'package:project_x/modules/employees/ui/widgets/page_of_filter.dart';
import 'package:project_x/modules/home/ui/screens/home_page.dart';
import 'package:project_x/modules/login_page/ui/screens/login_page.dart';
import 'package:project_x/modules/profile_page/ui/screens/profile_description.dart';
import 'package:project_x/modules/profile_page/ui/screens/profile_page.dart';

export 'routess.gr.dart';

@MaterialAutoRouter(
  routes: <AutoRoute>[
    AutoRoute(
      page: LoginPage,
      path: '/',
    ),
    AutoRoute(
      page: HomePage,
      path: '/home',
      children: [
        AutoRoute(path: 'home', page: EmployeesPage),
      ],
    ),
    AutoRoute(
      page: ProfilePage,
      path: '/profile',
    ),
    AutoRoute(
      page: ProfileDescription,
      path: '/description',
    ),
    AutoRoute(
      page: PageFiltering,
      path: '/filter',
    ),
  ],
)
class $AppRouter {}
