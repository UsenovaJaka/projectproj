import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';

class DioSettings {
  DioSettings() {
    setup();
  }
  final _dio = Dio(
    BaseOptions(
      baseUrl: 'http://45.134.254.89:8080/api/',
      headers: {'Content-Type': 'application/json'},
    ),
  );

  Dio get dio => _dio;
  final _storage = GetIt.I<FlutterSecureStorage>();

  void setup() {
    final interceptors = dio.interceptors;
    interceptors.clear();

    final headerInterceptor =
        QueuedInterceptorsWrapper(onRequest: (options, handler) async {
      final accessToken = await _storage.read(key: 'accessToken');
      log(accessToken.toString());
      if (accessToken != null) {
        options.headers['Authorization'] = 'Bearer $accessToken';
      }

      handler.next(options);
    }, onError: (DioError error, ErrorInterceptorHandler handler) async {
      if (error.response?.statusCode == 401) {
        await _storage.deleteAll();
        if (await _storage.containsKey(key: 'refreshToken')) ;
      }
      handler.next(error);
    });

    final logInterceptor = LogInterceptor(
      requestHeader: true,
      error: true,
      request: true,
      requestBody: true,
      responseBody: true,
      responseHeader: true,
    );

    interceptors.addAll([if (kDebugMode) headerInterceptor, logInterceptor]);
  }
}

@module
abstract class NetworkModule {
  @lazySingleton
  Dio get dio => DioSettings().dio;
  FlutterSecureStorage get storage => FlutterSecureStorage();
}
