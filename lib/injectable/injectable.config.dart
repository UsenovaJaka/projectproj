// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: unnecessary_lambdas
// ignore_for_file: lines_longer_than_80_chars
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:dio/dio.dart' as _i3;
import 'package:flutter_secure_storage/flutter_secure_storage.dart' as _i4;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import '../dio_settings/dio_settings.dart' as _i12;
import '../modules/employees/api_service/empl_api.dart' as _i6;
import '../modules/employees/core/bloc/employees_bloc.dart' as _i10;
import '../modules/employees/core/repo/repo_emp.dart' as _i7;
import '../modules/login_page/api_service/auth_api.dart' as _i5;
import '../modules/login_page/core/bloc/auth_bloc.dart' as _i11;
import '../modules/login_page/core/provider/login_provider.dart' as _i8;
import '../modules/login_page/core/repository/auth_repo.dart' as _i9;

// ignore_for_file: unnecessary_lambdas
// ignore_for_file: lines_longer_than_80_chars
// initializes the registration of main-scope dependencies inside of GetIt
_i1.GetIt $initGetIt(
  _i1.GetIt getIt, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) {
  final gh = _i2.GetItHelper(
    getIt,
    environment,
    environmentFilter,
  );
  final networkModule = _$NetworkModule();
  gh.lazySingleton<_i3.Dio>(() => networkModule.dio);
  gh.factory<_i4.FlutterSecureStorage>(() => networkModule.storage);
  gh.lazySingleton<_i5.ApiClient>(() => _i5.ApiClient(gh<_i3.Dio>()));
  gh.lazySingleton<_i6.ApiService>(() => _i6.ApiService(gh<_i3.Dio>()));
  gh.lazySingleton<_i7.EmployeesRepository>(
      () => _i7.EmployeesRepository(gh<_i6.ApiService>()));
  gh.lazySingleton<_i8.LoginProvider>(() => _i8.LoginProvider(
        gh<_i5.ApiClient>(),
        gh<_i4.FlutterSecureStorage>(),
      ));
  gh.lazySingleton<_i9.AuthRepository>(
      () => _i9.AuthRepository(gh<_i8.LoginProvider>()));
  gh.factory<_i10.EmployeesBloc>(
      () => _i10.EmployeesBloc(gh<_i7.EmployeesRepository>()));
  gh.factory<_i11.AuthBloc>(() => _i11.AuthBloc(gh<_i9.AuthRepository>()));
  return getIt;
}

class _$NetworkModule extends _i12.NetworkModule {}
