import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:project_x/const/app_colors.dart';
import 'package:project_x/injectable/injectable.dart';
import 'package:project_x/modules/employees/api_service/empl_api.dart';
import 'package:project_x/routess/routess.gr.dart';
import 'package:encrypt/encrypt.dart' as encrypt;

import 'package:project_x/widgets/app_unfocus.dart';

void main() {
  configureInjection(Env.dev);

  runApp(const MyApp());
}

final appRouter = AppRouter();

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return AppUnfocuser(
      child: AnnotatedRegion(
        value: SystemUiOverlayStyle(
          statusBarColor: AppColors.bg,
          systemNavigationBarColor: AppColors.black,
        ),
        child: MaterialApp.router(
          theme: ThemeData(
            fontFamily: "NunitoSans",
          ),
          debugShowCheckedModeBanner: false,
          routeInformationParser: appRouter.defaultRouteParser(),
          routerDelegate: appRouter.delegate(),
          title: 'Material App',
        ),
      ),
    );
  }
}
